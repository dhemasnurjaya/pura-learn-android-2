package com.dhemas.app.tabapp.Request;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * TabApp by dhemas
 * created on: 05/04/2017
 */

public abstract class RequestDataKaryawan implements Response.Listener<String>, Response.ErrorListener {

    private RequestQueue requestQueue;
    private StringRequest stringRequest;

    public RequestDataKaryawan(Context context) {
        this.requestQueue = Volley.newRequestQueue(context);
        stringRequest = new StringRequest(
                Request.Method.GET,
                "http://93.188.167.64:8080/pura-android-svc/getkaryawan",
                this, this
        );
    }

    public void sendRequest(){
        requestQueue.add(stringRequest);
    }
}
