package com.dhemas.app.tabapp;

/**
 * TabApp by dhemas
 * created on: 05/04/2017
 */

public class Karyawan {

    public String ID_KARYAWAN;
    public String NAMA;
    public String ALAMAT;

    public Karyawan() {
    }

    public Karyawan(String ID_KARYAWAN, String NAMA, String ALAMAT) {
        this.ID_KARYAWAN = ID_KARYAWAN;
        this.NAMA = NAMA;
        this.ALAMAT = ALAMAT;
    }
}
