package com.dhemas.app.tabapp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.dhemas.app.tabapp.R;
import com.dhemas.app.tabapp.Request.RequestNewKaryawan;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentB extends Fragment implements View.OnClickListener {

    private EditText input_nik;
    private EditText input_nama;
    private EditText input_alamat;
    private Button btn_insert;

    public FragmentB() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View parent = inflater.inflate(R.layout.fragment_b, container, false);

        input_nik = (EditText) parent.findViewById(R.id.input_nik);
        input_nama = (EditText) parent.findViewById(R.id.input_nama);
        input_alamat = (EditText) parent.findViewById(R.id.input_alamat);
        btn_insert = (Button) parent.findViewById(R.id.btn_insert);

        btn_insert.setOnClickListener(this);

        return parent;
    }

    private void inputData(){
        String nik = input_nik.getText().toString();
        String nama = input_nama.getText().toString();
        String alamat = input_alamat.getText().toString();

        Map<String, String> map = new HashMap<>();
        map.put("id_karyawan", nik);
        map.put("nama", nama);
        map.put("alamat", alamat);

        RequestNewKaryawan request = new RequestNewKaryawan(getActivity(), map) {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("--response", error.toString());
            }

            @Override
            public void onResponse(String response) {
                Log.i("--response", response);
                if(response.contains("success")){

                } else {

                }
            }
        };

        request.sendRequest();
    }

    @Override
    public void onClick(View view) {
        inputData();
    }
}
