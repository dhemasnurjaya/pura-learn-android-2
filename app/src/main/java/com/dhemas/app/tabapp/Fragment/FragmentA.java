package com.dhemas.app.tabapp.Fragment;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.VolleyError;
import com.dhemas.app.tabapp.Adapter.AdapterListKaryawan;
import com.dhemas.app.tabapp.DialogActionKaryawan;
import com.dhemas.app.tabapp.Karyawan;
import com.dhemas.app.tabapp.R;
import com.dhemas.app.tabapp.Request.RequestDataKaryawan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.dhemas.app.tabapp.Utility.U;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentA extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemLongClickListener {

    private ListView listViewKaryawan;
    private SwipeRefreshLayout swipeRefreshLayout;

    private DialogActionKaryawan dialog;

    public FragmentA() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View parentView = inflater.inflate(R.layout.fragment_a, container, false);

        listViewKaryawan = (ListView) parentView.findViewById(R.id.listview_karyawan);
        swipeRefreshLayout = (SwipeRefreshLayout) parentView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        listViewKaryawan.setOnItemLongClickListener(this);

        //dialog
        dialog = new DialogActionKaryawan(getActivity());

        return parentView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //data fetch
        swipeRefreshLayout.setRefreshing(true);
        fetchData();
        bindData();
    }

    private void fetchData(){
        RequestDataKaryawan request = new RequestDataKaryawan(getActivity()){
            @Override
            public void onResponse(String response) {
                swipeRefreshLayout.setRefreshing(false);
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject object = jsonArray.getJSONObject(i);
                        U.listKaryawan.add(new Karyawan(
                                object.getString("ID_KARYAWAN"),
                                object.getString("NAMA"),
                                object.getString("ALAMAT")
                        ));
                    }

                    U.adapterKaryawan.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                        .setTitle("WARNING")
                        .setMessage(error.toString())
                        .setPositiveButton("OK", null);

                builder.create().show();
            }
        };

        U.listKaryawan.clear();
        //progressDialog.setMessage("Loading data");
        //progressDialog.show();
        request.sendRequest();
    }

    private void bindData(){
        U.setAdapterKaryawan(getActivity());
        listViewKaryawan.setAdapter(U.adapterKaryawan);
    }

    @Override
    public void onRefresh() {
        fetchData();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
        dialog.setIDKaryawan(U.listKaryawan.get(position).ID_KARYAWAN);
        dialog.getDialog().show();

        return true;
    }
}
