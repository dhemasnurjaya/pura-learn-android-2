package com.dhemas.app.tabapp;

import android.content.Context;

import com.android.volley.VolleyError;
import com.dhemas.app.tabapp.Adapter.AdapterListKaryawan;
import com.dhemas.app.tabapp.Request.RequestDataKaryawan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * TabApp by dhemas
 * created on: 07/04/2017
 */

public class Utility {
    public static final Utility U = new Utility();

    public ArrayList<Karyawan> listKaryawan = new ArrayList<>();
    public AdapterListKaryawan adapterKaryawan;

    public void setAdapterKaryawan(Context context){
        adapterKaryawan = new AdapterListKaryawan(listKaryawan, context);
    }

    public void fetchData(Context context){
        RequestDataKaryawan request = new RequestDataKaryawan(context){
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject object = jsonArray.getJSONObject(i);
                        U.listKaryawan.add(new Karyawan(
                                object.getString("ID_KARYAWAN"),
                                object.getString("NAMA"),
                                object.getString("ALAMAT")
                        ));
                    }

                    U.adapterKaryawan.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };

        U.listKaryawan.clear();
        //progressDialog.setMessage("Loading data");
        //progressDialog.show();
        request.sendRequest();
    }

}
