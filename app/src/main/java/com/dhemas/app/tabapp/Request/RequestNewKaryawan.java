package com.dhemas.app.tabapp.Request;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * TabApp by dhemas
 * created on: 06/04/2017
 */

public abstract class RequestNewKaryawan implements Response.Listener<String>, Response.ErrorListener {

    private RequestQueue requestQueue;
    private StringRequest stringRequest;

    public RequestNewKaryawan(Context context, final Map<String, String> params) {
        this.requestQueue = Volley.newRequestQueue(context);
        stringRequest = new StringRequest(
                Request.Method.POST,
                "http://93.188.167.64:8080/pura-android-svc/newkaryawan",
                this, this
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
    }

    public void sendRequest(){
        requestQueue.add(stringRequest);
    }

}
